schema "movie-ratings" {
  charset = "utf8mb4"
  collate = "utf8mb4_0900_ai_ci"
}

table "movies" {
  schema = schema.movie-ratings
  column "id" {
    null = false
    type = int
    auto_increment = true
  }
  column "title" {
    null = true
    type = varchar(255)
  }
  column "genre" {
    null = true
    type = varchar(255)
  }
  column "year" {
    null = true
    type = int
  }

  primary_key {
    columns = [column.id]
  }
}

table "reviewers" {
  schema = schema.movie-ratings
  column "id" {
    null = false
    type = int
    auto_increment = true
  }
  column "first_name" {
    null = true
    type = varchar(255)
  }
  column "last_name" {
    null = true
    type = varchar(255)
  }
  primary_key {
    columns = [column.id]
  }
}

table "ratings" {
  schema = schema.movie-ratings
  column "rating" {
    null = false
    type = int
  }
  column "reviewer_id" {
    type = int
  }
  column "movie_id" {
    type = int
  }
  foreign_key "reviewer_id" {
    columns     = [column.reviewer_id]
    ref_columns = [table.reviewers.column.id]
  }
  foreign_key "movie_id" {
    columns     = [column.movie_id]
    ref_columns = [table.movies.column.id]
  }
  primary_key {
    columns = [column.movie_id, column.reviewer_id]
  }
}
